#include "metric1.h"

metric1::metric1()
{
       file = new QFile("D:\\a.cpp");
       countS = 0;
       countemptys = 0;
       countcomments = 0;
       countscomment = 0;
       countuniqoperators = 0;
       countuniqoperands = 0;
       countoperators = 0;
       countoperands = 0;
       countsep = 0;
       treelevel = 0;
}


void  metric1::initOperators()
{
    int i = 0;
    QFile op("D:\\operators.txt");

    if (op.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream in(&op);
        while (!in.atEnd())
        {
            operators.push_back(in.readLine());
            i++;
        }
        op.close();
    }
}

void  metric1::setCountS()
{
    if (file->open(QIODevice::ReadOnly | QIODevice::Text))
    {
       countS = QTextStream(file).readAll().split('\n').count();
       file->close();
    }
}

void  metric1::setcountoperands()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            QStringList s;
            if(line.contains("+"))
            {
                s = line.split("+");
                if (!uniqOperands.contains(s[0]))
                    uniqOperands.append(s[0]);
                if (!uniqOperands.contains(s[1]))
                    uniqOperands.append(s[1]);
                countoperands+=2;
            }
            if(line.contains("-"))
            {
                s = line.split("-");
                if (!uniqOperands.contains(s[0]))
                    uniqOperands.append(s[0]);
                if (!uniqOperands.contains(s[1]))
                    uniqOperands.append(s[1]);
                countoperands+=2;
            }
            if(line.contains("*"))
            {
                s = line.split("*");
                if (!uniqOperands.contains(s[0]))
                    uniqOperands.append(s[0]);
                if (!uniqOperands.contains(s[1]))
                    uniqOperands.append(s[1]);
                countoperands+=2;
            }
            if(line.contains("/"))
            {
                s = line.split("/");
                if (!uniqOperands.contains(s[0]))
                    uniqOperands.append(s[0]);
                if (!uniqOperands.contains(s[1]))
                    uniqOperands.append(s[1]);
                countoperands+=2;
            }
            if(line.contains("="))
            {
                s = line.split("=");
                if (!uniqOperands.contains(s[0]))
                    uniqOperands.append(s[0]);
                if (!uniqOperands.contains(s[1]))
                    uniqOperands.append(s[1]);
                countoperands+=2;
            }

             countuniqoperands = uniqOperands.toSet().count();
            //Конец подсчёта операндов
        }
        file->close();
    }
}

/*void  metric1::calcOperands(QRegExp reg, QString line)
{
    if (line.contains(reg))
    {
        int pos = reg.indexIn(line);
        if(pos > -1)
        {
            if (!uniqOperands.contains(reg.cap(0)))
                uniqOperands.append(reg.cap(0));
         //   if (!uniqOperands.contains(reg.cap(1)))
         //       uniqOperands.append(reg.cap(1));
        }
        countoperands+=2;
        file->close();
    }
}*/

void  metric1::setcountemptys()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if(line.trimmed().isEmpty())
                countemptys++;
        }
        file->close();
    }
}

void  metric1::setcountscomment()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.contains("//"))
               countscomment++;
        }
        file->close();
    }
}

void  metric1::setpersent()
{
    persent = countcomments*100/countS;
}

void  metric1::setcountoperators()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            for(int i=0;i<operators.toSet().count();i++)
            {
                if (line.contains(operators[i]))
                {
                    countoperators++;
                    if(!uniqOperators.contains(operators[i]))
                        uniqOperators.append(operators[i]);
                }
            }
            countuniqoperators = uniqOperators.toSet().count();
        }
        file->close();
    }
}

void  metric1::setcountsep()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.contains(";"))
               countsep++;
        }
        file->close();
    }
}

void  metric1::setMaxTreeLevel()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.contains("if") || line.contains("else") ||
                line.contains("else if") || line.contains("goto") ||
                line.contains("case"))
               treelevel++;
        }
        file->close();
    }
}

QString  metric1::print()
{
    initOperators();
    setCountS();
    setcountemptys();
    setcountcomments();
    setcountscomment();
    setpersent();

    setcountoperators();
    setcountoperands();

    setcountsep();
    setMaxTreeLevel();


    short VocabluaryOperators = countuniqoperators+countsep;
    short ProgramLength = countoperands+countoperators;
    short ProgramVolume = (countoperands+countoperators)*log2(countuniqoperands+countuniqoperators);
    short CyklComplexity = 2-4+2*treelevel;
    QString str = "";
    str+= "Количество строк кода: ";
    str+= QString::number(countS);
    str+= '\n';
    str+="Количество пустых строк: ";
    str+=QString::number(countemptys);
    str+='\n';
    str+="Количество строк с комментариями: ";
    str+=QString::number(countcomments);
    str+='\n';
    str+="Количество строк с кодом и комментариями: ";
    str+=QString::number(countscomment);
    str+='\n';
    str+="Процент комментариев: ";
    str+=QString::number(persent);
    str+='\n';
    str+="Словарь операторов: ";
    str+=QString::number(VocabluaryOperators);
    str+='\n';
    str+= "Общее количество операторов: ";
    str+=QString::number(countoperators);
    str+='\n';
    str+="Словарь операндов: ";
    str+=QString::number(countuniqoperands);
    str+='\n';
    str+="Общее количество операндов: ";
    str+=QString::number(countoperands);
    str+='\n';
    str+="Длина программы: ";
    str+=QString::number(ProgramLength);
    str+='\n';
    str+="Объем программы: ";
    str+=QString::number(ProgramVolume);
    str+='\n';
    str+="Цикломатическая сложность программы: ";
    str+=QString::number(treelevel);
    str+='\n';
    return str;
}

void  metric1::setcountcomments()
{
    if (file->open(QIODevice::ReadOnly| QIODevice::Text))
    {
        QTextStream in(file);
        while (!in.atEnd())
        {
            QString line = in.readLine();
            if (line.trimmed().startsWith("//"))
               countcomments++;
        }
        file->close();
    }
}
