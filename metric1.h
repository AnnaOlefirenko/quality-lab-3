#ifndef METRIC1_H
#define METRIC1_H

#include <QObject>
#include <QCoreApplication>
#include <QFile>
#include <QTextStream>
#include <QStringList>
#include <QDebug>
#include <QTextCodec>
#include <math.h>

class metric1
{
private:
    QFile *file;
    short countS;
    short countemptys;
    short countcomments;
    short countscomment;
    short persent;
    short countuniqoperators;
    short countuniqoperands;
    short countoperators;
    short countoperands;
    short countsep;
    short treelevel;
    QStringList operators;
    QStringList uniqOperators;
    QStringList uniqOperands;
public:
    metric1();
    void initOperators();
    void setCountS();
    void setcountemptys();
    void setcountcomments();
    void setcountscomment();
    void setpersent();

    void setcountoperators();
    void setcountoperands();
  //  void calcOperands(QRegExp reg, QString line);

    void setcountsep();
    void setMaxTreeLevel();

    QString print();
};

#endif // METRIC1_H
