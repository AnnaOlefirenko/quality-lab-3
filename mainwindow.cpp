#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QDebug>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    metric1 m;
    QString str = m.print();
    ui->textBrowser->setText(str);
}

MainWindow::~MainWindow()
{
    delete ui;
}
